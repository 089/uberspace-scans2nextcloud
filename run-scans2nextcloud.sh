#!/bin/bash

cd ~/bin/uberspace-scans2nextcloud

# update repository
## Fetch the latest changes from the remote repository
git fetch origin

## Reset the local branch to match the remote branch
git reset --hard origin/main

# Set the source directory
SCANS_DIR="/home/$USER/SCANS"

# Set the log file name
LOG_FILENAME="log.txt"
echo "" > "${LOG_FILENAME}"

# upload to public nextcloud share
touch config.sh
source config.sh

# Loop durch Dateien im Scan-Verzeichnis
for scanned_file in "${SCANS_DIR}"/*.*; do
    if [ -f "${scanned_file}" ]; then
        # Extrahiere den Dateinamen
        filename=$(basename "${scanned_file}")

        # Führe den Curl-Befehl aus, um die Datei hochzuladen
        curl -k -T "${scanned_file}" -u "${NEXTCLOUD_PUBLIC_USER}:" -H 'X-Requested-With: XMLHttpRequest' "${NEXTCLOUD_URL}${filename}"
        curl_exit_code=$?

        # Überprüfe den Erfolg des Curl-Befehls
        if [ $curl_exit_code -eq 0 ]; then
            echo "Upload erfolgreich: $filename" >> "${LOG_FILENAME}"

            # Lösche die Datei nach erfolgreichem Upload
            rm "$scanned_file"
            echo "Datei auf Zwischenspeicher gelöscht: ${filename}" >> "${LOG_FILENAME}"
        else
            echo "Fehler beim Upload auf Nextcloud von $filename. Curl exit code: $curl_exit_code" >> "${LOG_FILENAME}"
        fi
    fi
done

# send mail 
{
  echo -e "Dies ist die Liste der neuen Scans:\n"
  ls "${SCANS_DIR}"
  echo -e "\n\nInhalt der Logdatei:\n"
  cat "${LOGFILE}"
} | mail -s "Neue SCANS" "${MAILADDRESS}"
